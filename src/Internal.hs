{-# LANGUAGE TemplateHaskell #-}

module Internal where

import           Control.Concurrent
import qualified Data.ByteString.Lazy    as BL
import           Data.Global
import           Data.IORef              (readIORef, writeIORef)
import           Data.List               as L
import           Data.Maybe              as M
import           Data.String
import           Data.Text               as T
import           Data.Text.Conversions   (convertText)
import           Data.Time.Clock
import           Protolude
import           System.Environment      as E
import           Text.Read               (read)

-- aeson
import           Data.Aeson

-- http
import           Network.HTTP.Client     (newManager)
import           Network.HTTP.Client.TLS (tlsManagerSettings)
import           Network.HTTP.Conduit

-- telegram-api
import           Web.Telegram.API.Bot

-- internal library
import           Types

declareIORef "lastEventId" [t|Int|] [|0|]

-- | Start World Cup 2018 Bot.
startBot :: IO ()
startBot = forever $ do
  start <- getCurrentTime

  -- matchURL <- E.getEnv "MATCH_CURRENT_URL"
  matchURL <- E.getEnv "MATCH_TODAY_URL"
  matchResponse <- lookupTodayMatch matchURL
  let decodedMatch = eitherDecode matchResponse :: Either String [Match]
  case decodedMatch of
    Left err      -> putStrLn err
    Right matches -> case L.filter (\x -> matchStatus x == "in progress") matches of
                       m:_ -> processMatch m
                       _   -> print "There is no match right now"

  end <- getCurrentTime
  let diff = diffUTCTime end start
      usecs = floor (toRational diff * 1000000) :: Int
      delay = 10000*1000 - usecs
  if delay > 0
    then threadDelay delay
    else return ()

-- | Lookup today's `Match`.
lookupTodayMatch :: String -> IO BL.ByteString
lookupTodayMatch url = simpleHttp url

-- | Process "in progress" `Match`.
processMatch :: Match -> IO ()
processMatch match = do
  eid <- readIORef lastEventId
  print eid -- remove this when stable
  let events = matchHomeTeamEvents match ++ matchAwayTeamEvents match
  case L.filter (\x -> eventId x > eid) events of
    event:_ -> do
      sendEvent match event
      writeIORef lastEventId $ eventId event
    _   -> print "There is no valid match"

-- | Send `Event` to telegram.
sendEvent :: Match -> Event -> IO ()
sendEvent match event = do
  token <- E.getEnv "TELEGRAM_TOKEN"
  chatId <- E.getEnv "TELEGRAM_CHAT_ID"

  -- Prepare token and chatId
  let token' = Token $ convertText $ "bot" ++ token
      chatId' = ChatId (read chatId :: Int64)

  -- Prepare message request
  let newEvent = "*New Event: " <> eventTypeOfEvent event <> "*"
      player = "Player: " <> eventPlayer event
      at = "Occurs at: " <> eventTime event
      summary = teamCountry (matchHomeTeam match) <> " (" <> (show $ teamGoals (matchHomeTeam match)) <> ")"
             <> " - "
             <> teamCountry (matchAwayTeam match) <> " (" <> (show $ teamGoals (matchAwayTeam match)) <> ")"
      messages = [ newEvent
                 , player
                 , at
                 , "-"
                 , summary
                 ] :: [Text]
      message = T.unlines messages
      request = SendMessageRequest
                  chatId'
                  message
                  (Just Markdown)
                  Nothing
                  Nothing
                  Nothing
                  Nothing

  -- Send message request
  manager <- newManager tlsManagerSettings
  response <- sendMessage token' request manager
  case response of
    Left err  -> print err
    Right res -> putStrLn $ M.fromJust $ text $ result res

# bot-world-cup

This program will continously get current world cup match and send events to Telegram every 10 seconds.
Handling duplicate event is still on progress.

World cup API provided by [http://worldcup.sfg.io](https://github.com/estiens/world_cup_json).

## Setup

```shell
# Setup project
stack setup

# Do an initial build of the project
stack build

# Needed for haskero (vscode plugin)
stack build intero

# Needed for Haskell GHCi Debug Adapter Phoityne (vscode plugin)
stack install phoityne-vscode

# Build the project
stack build
```

## Post Setup

### Build Executable

```shell
make build
```

Executable can be found in `.stack-work/install/x86_64-linux-tinfo6/lts-11.14/8.2.2/bin` folder.

### Build Documentation

```shell
make doc
```

Documentation can be found in `docs` folder.

### Usage

```shell
stack exec bot-world-cup

# or

.stack-work/install/x86_64-linux-tinfo6/lts-11.14/8.2.2/bin/bot-world-cup
```